const express = require('express');
const nunjucks = require('nunjucks');

const app = express();

nunjucks.configure('views', {
  autoescape: true,
  express: app,
  watch: true
})

// template config
app.set('view engine', 'njk')
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}))

const checkAgeQueryParam = (req, res, next) => {
  const {
    age
  } = req.query

  if (!age) {
    return res.redirect('/')
  }

  return next()
}

app.get('/', (req, res) => {
  return res.render('start')
})

app.get('/major', checkAgeQueryParam, (req, res) => {
  const {
    age
  } = req.query

  return res.render('major', {
    age
  })
})

app.get('/minor', checkAgeQueryParam, (req, res) => {
  const {
    age
  } = req.query

  return res.render('minor', {
    age
  })
})

app.post('/check', (req, res) => {
  const {
    age
  } = req.body

  if (age >= 18) {
    return res.redirect(`/major?age=${age}`)
  } else {
    return res.redirect(`/minor?age=${age}`)
  }
})

//code duplicate
app.post('/check', (req, res) => {
  const {
    age
  } = req.body

  if (age >= 18) {
    return res.redirect(`/major?age=${age}`)
  } else {
    return res.redirect(`/minor?age=${age}`)
  }
})

require('dotenv').config();

app.listen(process.env.PORT || 3000, () => console.log('Server is running...'));